# Elementary Principles of Philosophy

Text taken from readmarxeveryday.org

## Usage

The tools required are pandoc and sigil.

Do `make epub`. Open the produced epub file in sigil and press F6 to split
chapters.

TODO Make a lua filter to add sigil break `<hr>`s.
